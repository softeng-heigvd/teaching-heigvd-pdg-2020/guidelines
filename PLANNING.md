# Planning

|      | Date       | Focus                                                        |
| ---- | ---------- | ------------------------------------------------------------ |
| 1    | 14.09.2020 | Introduce **objectives** and **guidelines**. <br />Present and experiment with **rapid cycle workflow**. <br />Create **groups** and **ideate**. Describe and assign **roles** within the teams.<br />Define the **long term** vision and the **MVP** scope (the goal is to deliver the MVP at the end of the semester). <br />At the end of the session, **every group must be able to present the project idea**. Every team member must have used the **rapid workflow** to write a document where he/she describes his perspective on the project idea. |
| 2    | 21.09.2020 | Specify the **scenarios** that will be used to **demo the product** at the end of the semester. <br />Create **mockups** for the UI/UX.<br />Create a **landing page** to present the product idea. Every team needs to get a DOMAIN NAME before that. |
| 3    | 28.09.2020 | **Refine the demo scenario** and **mockups** for the UI/UX.<br />Improve the landing page. |
| 4    | 05.10.2020 | **First presentation**. One person in every group presents the project idea and does a "fake demo" using the mockups in **10 minutes**. <br />**Architecture and planning for the first development iteration**. Pick the **technology stack** (what will be used in the backend, in the frontend and to implement the build pipeline). For instance: Springboot in the backend, Vue.js in the front-end and GitLab pipelines for the CI. **Pick a simple functional slice of the system to be implemented in 4 weeks**. |
| 5    | 12.10.2020 | **CI/CD**: setup the initial pipeline to build and deploy the service in the production environment. |
|      |            |                                                              |
| 6    | 26.10.2020 | **Iteration 1**                                              |
| 7    | 02.11.2020 | **Iteration 1**                                              |
| 8    | 09.11.2020 | **Iteration 1**                                              |
| 9    | 16.11.2020 | **Iteration 1**                                              |
| 10   | 23.11.2020 | **Second presentation**. In 10 minutes, one person presents the structure of the GitLab repo, explains how the back-end and front-end is automatically built and deployed to the production environment. The person also explains which automated tests have been created by the back-end and front-end developers and how they are run in the pipeline.<br/>**Iteration 2** |
| 11   | 30.11.2020 | **Iteration 2**                                              |
| 12   | 07.12.2020 | **Iteration 2**                                              |
| 13   | 14.12.2020 | **Iteration 2**                                              |
|      |            |                                                              |
|      |            |                                                              |
| 14   | 04.01.2021 | Preparation of the final demo, documentation and video       |
| 15   | 11.01.2021 | Preparation of the final demo, documentation and video       |
| 16   | 18.01.2021 | **Final presentations**. In 10 minutes, present the product demo (pitch). Then in 10 minutes present the implementation (architecture, pipeline, etc.) |

## Workload

"Périodes encadrées: 16 x 4 = 64 périodes, soit 48 heures."

"Charge de travail totale: 90 heures (soit 42 heures en plus des périodes encadrées)."

90 hours ~= 2 weeks of work full time

* I have 2 weeks to implement a back-end, where I expose a domain model via a REST API. I need time to design the model, the API. I need time to write automated tests.
* I have 2 weeks to implement the front-end for my application (mobile and/or desktop). I need to create atomic elements, which I can assemble in larger components and finally in pages. I need to manage the navigation between pages. I need to decide what kind of tests will make the development process efficient.
* I have 2 weeks to design and implement two runtime environments: one local and one in the cloud. I have to implement a CI/CD pipeline to automatically build and test the software, before releasing it into the cloud.
* I have 2 weeks to help my team mates stay deliver what is required to run the demo that will convey the vision of our product. Depending on the situation, I might spend time working in the back-end, the front-end or the runtime environments. But I need to maintain a global view on the progress of the project.

